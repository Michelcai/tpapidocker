package com.example.tpapi;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;

/**
 *
 */
public class Personnage  implements Serializable {
    @Id
    @Indexed
    private Long id;
    private String nom;
    private String prenom;

    public Personnage(Long id,String nom, String prenom) {
        this.id=id;
        this.nom = nom;
        this.prenom = prenom;
    }

    public Personnage(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}
