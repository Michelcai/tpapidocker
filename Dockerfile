FROM java:8-jdk-alpine
LABEL com.example.version="0.0.1-beta"
LABEL vendor="Michel cai"
LABEL com.example.release-date="2019-10-16"
COPY target/tpapi-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java","-Dspring.data.redis.uri=redis://redis","-Djava.security.egd=file:/dev/./urandom", "-jar", "tpapi-0.0.1-SNAPSHOT.jar"]